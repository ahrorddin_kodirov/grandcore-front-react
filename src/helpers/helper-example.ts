/* This is simple example of helper file
* ---- You should use one default export from one helper file
* ---- Don't forget to define types for your helper
* ---- Respect semantics
*/
export default function helperExample(firstArg: number, secondArg: number): number {
  return firstArg + secondArg
}
