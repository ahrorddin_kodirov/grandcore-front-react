import React, { FC } from 'react'
import Page from 'components/Page/Page'

interface IProps {}

const About: FC<IProps> = () => {
  return <Page>
    About Page is here...
  </Page>
}

export default About
