import React, { FC } from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import Header from 'components/Header/Header'
import Footer from 'components/Footer/Footer'
import Routes from 'routing/routes'
import { IRoute } from 'types/Route'
import styles from 'views/Main/Main.module.scss'

interface IProps {}

const Main: FC<IProps> = () => {
  return <Router>
    <Header />

    <main className={styles.main}>
      <Switch>
        <Route path='/' exact>
          <Redirect to='/about' />
        </Route>

        {Routes.map((route:IRoute, index:number) => <Route {...route} />)}
      </Switch>
    </main>

    <Footer />
  </Router>
}

export default Main
