import { IRoute } from 'types/Route'
import { hot } from 'react-hot-loader/root'
import Auth from 'views/Auth/Auth'
import About from 'views/About/About'

const routes: Array<IRoute> = [
  {
      exact: true,
      path: '/auth',
      component: hot(Auth)
  },
  {
      exact: true,
      path:'/about',
      component: hot(About)    
  }
]

export default routes