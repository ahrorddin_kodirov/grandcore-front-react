export enum HeadlineLevel {
  FIRST = 1,
  SECOND = 2,
  THIRD = 3
}
