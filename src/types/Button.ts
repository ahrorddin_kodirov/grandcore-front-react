export enum ButtonType {
  PRIMARY= 'primary',
  SECONDARY = 'secondary',
  SECONDARY_OUTLINE = 'secondary-outline'
}

export enum ButtonState {
  DEFAULT   = 'default',
  DISABLED = 'disabled'
}
