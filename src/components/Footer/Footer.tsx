import React, { FC } from 'react'
import styles from 'components/Footer/Footer.module.scss'

interface IProps {}

export const Footer: FC<IProps> = () => {
  return <footer className={styles.footer}>
    Footer works!
  </footer>
}

export default Footer
