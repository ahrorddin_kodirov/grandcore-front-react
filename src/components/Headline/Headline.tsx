import React, { FC } from 'react'
import { HeadlineLevel } from 'types/Headline'
import styles from 'components/Headline/Hadline.module.scss'

interface IProps {
  level: HeadlineLevel,
  text: string
}

const Headline: FC<IProps> = ({ level, text }) => {
  return <>
    { level === 1 && <h1 className={styles.first} children={text} /> }

    { level === 2 && <h2 className={styles.second} children={text} /> }

    { level === 3 && <h3 className={styles.third} children={text} /> }
  </>
}

export default Headline
