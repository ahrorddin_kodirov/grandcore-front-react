# GrandCore

*Первая в Мире Open Source корпорация. Универсальная платформа для любых открытых проектов*

- Задачи в Trello - https://trello.com/b/K73NeI1B/grandcore-front-react
- **Рабочий чат фронтендеров - https://t.me/joinchat/OQ6DaxmA5Xf7GERw8oZO8g** 
- Рабочий чат бекендеров -  https://t.me/joinchat/OQ6Da0w3cCxnfGl9K2Tv9g

- Наш сайт - https://grandcore.org
- Наши новости - https://t.me/grandcore_news
- Публичный чат - https://t.me/grandcore_chat
- Написать основателю - https://t.me/grandcore  **(если хотите в команду)**

- Текущие макеты в Figma - https://www.figma.com/file/NlikNEJQHliYlxI3MHhiSW/Share?node-id=2946%3A1036
- Репозиторий с компонентами и вёрсткой - https://gitlab.com/grandcore/stroika
- Репозиторий бекенда на Python - https://gitlab.com/grandcore/grandcore-backend-python


Вёрстка и API для MVP на бекенде уже готовы!

## Быстрый старт

1. Клонируйте репозиторий:
  ```shell
  $ git clone url.git
  ```

2. Установите зависимости:

  ```shell
  $ npm install
  ```

  *We recommend using `npm` for installing packages, but you can use `yarn` instead*:

  ```shell
  $ yarn install
  ```
3. Запуск локального сервера

  ```shell
  $ npm start
  ```

4. Сборка проекта
``` shell
$ npm run build
```

## Стек

* ReactJS
* TypeScript
* SCSS


# src
*корневая директория приложения*
### components 
*директория для компонентов приложения*
- 0
### media
*директория для графики, файлов и .т.п.*
- 0
### routes
*директория для описания роутинга*
- 0
### views
*директория для сформированных страниц*
- 0
### services
*директория для сервисов приложения*
- 0
### styles
*директория для стилей приложения*
-